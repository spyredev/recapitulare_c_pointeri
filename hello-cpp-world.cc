#include <iostream>
#include <assert.h> 

using namespace std;

struct Nod{
    int data;
    struct Nod * next;
};


// void functie (struct Nod * lista) ----> read only
// void functie (struct Nod ** lista) ---> poti modifica orice vrei la pointer-ul lista

void modificare(struct Nod * element){
    
    element->data = 100; // orice modificare interna pe elemnt este OK
    element->next = NULL;
}

void modificare2(struct Nod ** element){
    *element = new Nod;  // atat timp cat nu modificam cine este element in sine, dar putem trimite pointer la pointer 
    (*element)->data = 100;
    (*element)->next = NULL;
    
}

void modificare3(struct Nod * & element){
    element = new Nod;  
    element->data = 100;
    element->next = NULL;
    
}


void atribuireValoarePointer(struct Nod ** element){
    if(*element == NULL){
        cout <<"ELEMENTUL E NULL "<<endl;
    }else{
        cout <<"Elementul nu e null "<< endl;
    }
}

void functie(int * x){
    
    *x = 3333;
}

struct Nod * creareElement(int val){
    struct Nod * elementNou = new Nod;
    elementNou->data = val;
    return elementNou;
}

void afisareLista(struct Nod * primulElement){
    
    cout <<"[";
    while(primulElement != NULL){
        cout << primulElement->data  << ", ";
        primulElement = primulElement->next;
    }
    cout <<"]"<< endl;
}

void appendToList(struct Nod ** first, int val){
    
    
    if ((*first) == NULL){  // nu avem nimic in lista
        
        (*first) = new Nod;
        (*first)->data = val;
        (*first)->next = NULL;
        // cout << "FIRST VALUE: " << (*first)->data << endl;
    }else{
        // ....
        // mergem pana la penultimul element din lista si acolo adaugam noul element (dupa)
        struct Nod * bookmark = *first;
        while((bookmark)->next != NULL){
           bookmark = bookmark->next;
        }
        // penultimul
        bookmark->next = new Nod;
        bookmark->next->data = val;
        bookmark->next->next = NULL;
    }
}



int GetNth(struct Nod * head, int index)
{
    struct Nod* current = head;
    int count = 0; // the index of the node we're currently looking at
    while (current != NULL)
    {
        if (count == index) return(current->data);
        count++;
        current = current->next;
    }
    assert(0); // if we get to this line, the caller was asking
// for a non-existent element so we assert fail.
}



// Uses special case code for the head end
void SortedInsert(struct Nod** headRef, struct Nod* newNode)
{
// Special case for the head end
    if (*headRef == NULL || (*headRef)->data >= newNode->data)
    {
        newNode->next = *headRef;
        *headRef = newNode;
    }
    else
    {
// Locate the node before the point of insertion
        struct Nod* current = *headRef;
        while (current->next!=NULL && current->next->data<newNode->data)
        {
            current = current->next;
        }
        newNode->next = current->next;
        current->next = newNode;
    }
}





// Given a list, change it to be in sorted order (using SortedInsert()).
void sortare(struct Nod** headRef)
{
    struct Nod* result = NULL; // build the answer here
    struct Nod* current = *headRef; // iterate over the original list
    struct Nod* next;
    while (current!=NULL)
    {
        next = current->next; // tricky - note the next pointer before we change it
        SortedInsert(&result, current);
        current = next;
    }
    *headRef = result;
}


int main() {
    cout << "Hello World!" << endl;
    
    
    struct Nod * e1 = new Nod;
    e1->data = 200;
    
    // modificare2(&e1);
    modificare3(e1);
    
    
    
    
    cout << e1->data << endl;
    
    cout <<"====================================="<<endl;
    
    struct Nod * lista = creareElement(10);
    lista->next = creareElement(20);
    lista->next = creareElement(30);
    lista->next = creareElement(40);
    // cout << lista->data << endl;
    afisareLista(lista);
    
    cout <<"====================================="<<endl;
    struct Nod * list = NULL;  // Compilator issue !!!!! 
    
    // atribuireValoarePointer(&list);
    
    appendToList(&list, 11);  // if
    // struct Nod * bookmark = list;
    appendToList(&list, 2);  // else
    appendToList(&list, 3);  // else
    appendToList(&list, 4);  // else
    appendToList(&list, 5);  // else
    
    struct Nod * unPointerOarecare = list;
    unPointerOarecare = unPointerOarecare->next;
    cout << unPointerOarecare->data << endl;
    cout << list->data<<endl;
    
    
    cout <<"====================================="<<endl;
    cout << GetNth(list, 2) << endl;
    
    afisareLista(list);
    
    
    cout <<"===============exemplu sorted insert======================"<<endl;
    struct Nod * listaNoua = NULL;
    struct Nod * nn = creareElement(77);
    struct Nod * nn2 = creareElement(7);
    struct Nod * nn3 = creareElement(11);
    struct Nod * nn4 = creareElement(2000);
    
    SortedInsert(&listaNoua, nn);
    SortedInsert(&listaNoua, nn2);
    SortedInsert(&listaNoua, nn3);
    SortedInsert(&listaNoua, nn4);
    afisareLista(listaNoua);
    
     cout <<"===============verificare sortare======================"<<endl;
     struct Nod * listaNeSortata = NULL;
     appendToList(&listaNeSortata, 10);
     appendToList(&listaNeSortata, 17);
     appendToList(&listaNeSortata, 3);
     appendToList(&listaNeSortata, 1);
     afisareLista(listaNeSortata);
     
     sortare(&listaNeSortata);
     afisareLista(listaNeSortata);
    
}
