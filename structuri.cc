#include <iostream>
#include <assert.h> 
#include <string.h>

using namespace std;

struct Adresa{
    char * oras;
    char * strada;
    int numar;
    int codPostal;
    
    void atribuire(char * o, char * s, int n, int c){
        oras = new char[strlen(o)+1];
        strada = new char[strlen(s)+1];
        strcpy(oras, o);
        strcpy(strada, s);
        numar = n;
        codPostal = c;
    }
    
    void afisare(){
        cout <<"\tADRESA: oras = " << oras <<"  strada = " << strada << " numar = " << numar << " cod postal " << codPostal << endl;
    }
};

struct Student{
  char * nume;
  char * prenume;
  int varsta;
 
 
 Adresa adr;
 //  char * adresa;
 
  
  /*
  void atribuieAdresa(char * adr){
      adresa = new char[strlen(adr)+1];
      strcpy(adresa, adr);
  }*/
  
  void afisare(){
      cout <<"Nume: " << nume <<" varsta: " << varsta << endl;
  }
  
  void atribuire( char * n, char * p, int v){
      nume = new char[strlen(n) + 1];
      prenume = new char[strlen(p) + 1];
      strcpy(nume, n);
      strcpy(prenume, p);
      varsta = v;
  }
};


/*
void atribuire(struct Student * sn, char * n, char * p, int v){
    sn->nume = new char[strlen(n) + 1];
    sn->prenume = new char[strlen(p) + 1];
    strcpy(sn->nume, n);
    strcpy(sn->prenume, p);
    sn->varsta = v;
    // return sn;
}
*/
int main(){


    
    Student s1;
    s1.varsta = 23;
    s1.nume = new char[30];
    strcpy(s1.nume, "Ionel");
    
    s1.afisare();
    
    
    Student s2;
    // atribuire(&s2, "Vasile", "Vasi", 30);
    s2.atribuire("Vasilescu", "Vasile", 30);
    // s2.atribuieAdresa("Str. Zece Mese, nr 8 et 1 ap 2, Bucuresti, 37812"); NASOL
    s2.afisare();
    

    Student s3;
    s3.atribuire("Dumitrescu", "Dumitru", 30);
    // s3.atribuieAdresa("Bucuresti, Obor, sect. 2, 389210"); NASOL
    s3.afisare();
    
    
    Adresa a1;
    a1.atribuire("Bucuresti", "Zece Mese", 8, 893210);
    
    Adresa a2;
    a2.atribuire("Bucuresti", "Obor", 30, 93021);
    
    a1.afisare();
    a2.afisare();
    
    s3.adr = a1; // adr-ul lui s3 este a1
    
    cout << "RANDOM INFO : " << s3.adr.oras << endl;
    
    
    return 0;
}